CREATE TABLE Banda (
	nome VARCHAR(50) PRIMARY KEY,
	localDeOrigem VARCHAR(50) NOT NULL
);

CREATE TABLE Album (
	id SERIAL PRIMARY KEY,
	estilo VARCHAR(25) NOT NULL,
	banda VARCHAR(50) NOT NULL,
	anoDeLancamento DATE NOT NULL,
	FOREIGN KEY (banda) REFERENCES Banda(nome)
);

CREATE TABLE Artista (
	nome VARCHAR(50) PRIMARY KEY
);

CREATE TABLE Integrantes (
	nomeBanda VARCHAR(50),
	nomeArtista VARCHAR(50),
	PRIMARY KEY (nomeBanda, nomeArtista),
	FOREIGN KEY (nomeBanda) REFERENCES Banda(nome),
	FOREIGN KEY (nomeArtista) REFERENCES Artista(nome)
);