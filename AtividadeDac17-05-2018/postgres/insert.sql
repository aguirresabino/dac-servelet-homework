INSERT INTO Artista(nome) VALUES ('Wesley Vampirão');
INSERT INTO Artista(nome) VALUES('Zezin do Pagodi');
INSERT INTO Artista(nome) VALUES('Naruto UzuLaki');
INSERT INTO Artista(nome) VALUES('Sasuki Utirra');
INSERT INTO Artista(nome) VALUES('Neymarzão dus Forró');

INSERT INTO Banda(nome, localDeOrigem) VALUES ('Leleks de Konoha','Konoha');
INSERT INTO Banda(nome, localDeOrigem) VALUES ('Exalta Banga','Rio de Janeiro');
INSERT INTO Banda(nome, localDeOrigem) VALUES ('Cream Cracker','Londres');

INSERT INTO Integrantes(nomeBanda, nomeArtista) VALUES ('Leleks de Konoha', 'Naruto UzuLaki');
INSERT INTO Integrantes(nomeBanda, nomeArtista) VALUES ('Leleks de Konoha', 'Sasuki Utirra');
INSERT INTO Integrantes(nomeBanda, nomeArtista) VALUES ('Exalta Banga', 'Wesley Vampirão');
INSERT INTO Integrantes(nomeBanda, nomeArtista) VALUES ('Exalta Banga', 'Zezin do Pagodi');
INSERT INTO Integrantes(nomeBanda, nomeArtista) VALUES ('Cream Cracker', 'Neymarzão dus Forró');

