/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.dao;

import br.com.ifpb.modelo.Album;
import br.com.ifpb.modelo.Banda;
import br.com.ifpb.modelo.Estilo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aguirresabino
 */
public class AlbumDao {
    private Connection con = null;
    private PreparedStatement stmt = null;
    
    public boolean create(Album album) {
        int resultado = 0;
        try {
            con = this.getConnection();
            
            String sql = "INSERT INTO Album(Estilo, Banda, anoDeLancamento) VALUES(?, ?, ?)";

            stmt = con.prepareStatement(sql);
            stmt.setString(1, album.getEstilo().toString());
            stmt.setString(2, album.getBanda().getNome());
            stmt.setObject(3, album.getAnoDeLancamento());

            resultado = stmt.executeUpdate();

            stmt.close();
            con.close();

        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }        
        return resultado > 0;
    }
    
    public List<Album> read() throws SQLException, ClassNotFoundException {
        List<Album> albuns = null;
        con = this.getConnection();

        String sql = "SELECT * FROM Album";
        stmt = con.prepareStatement(sql);

        ResultSet rs = stmt.executeQuery();

        albuns = new ArrayList<>();
        while (rs.next()) {
            Album album = new Album();
            
            album.setAnoDeLancamento(rs.getObject("AnoDeLancamento", LocalDate.class));
            Banda banda = new Banda();
            banda.setNome(rs.getString("Banda"));
            album.setBanda(banda);
            album.setEstilo(Estilo.valueOf(rs.getString("Estilo")));
            album.setId(rs.getInt("Id"));

            albuns.add(album);
        }

        stmt.close();
        con.close();
        
        return albuns;
    }
    
    public boolean update(Album album) throws ClassNotFoundException, SQLException {
        int resultado = 0;
        con = this.getConnection();

        String sql = "UPDATE Album SET  Estilo = ?, Banda = ?, anoDeLancamento = ?"
                + "     WHERE id = ?";

        stmt = con.prepareStatement(sql);
        stmt.setString(1, album.getEstilo().toString());
        stmt.setString(2, album.getBanda().getNome());
        stmt.setObject(3, album.getAnoDeLancamento());
        stmt.setInt(4, album.getId());

        resultado = stmt.executeUpdate();

        stmt.close();
        con.close();
        
        return resultado > 0;
    }
    
    public boolean delete(Album album) throws ClassNotFoundException, SQLException {
        int resultado = 0;
        con = this.getConnection();

        String sql = "DELETE FROM Album WHERE id = ?";
        stmt = con.prepareStatement(sql);

        stmt.setInt(1, album.getId());

        resultado = stmt.executeUpdate();

        stmt.close();
        con.close();
        
        return resultado > 0;
    }
    
    private Connection getConnection() throws ClassNotFoundException, SQLException {
        return new Conexao().getConnection();
    }
}
