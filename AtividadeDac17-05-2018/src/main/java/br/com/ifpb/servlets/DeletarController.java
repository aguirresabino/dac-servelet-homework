package br.com.ifpb.servlets;

import br.com.ifpb.dao.AlbumDao;
import br.com.ifpb.modelo.Album;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeletarController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AlbumDao albumDao = new AlbumDao();
        Album album = new Album();

        int idAlbum = Integer.parseInt(request.getParameter("idBanda"));
        album.setId(idAlbum);

        try {
            albumDao.delete(album);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/listar");
        dispatcher.forward(request, response);
    }
}
