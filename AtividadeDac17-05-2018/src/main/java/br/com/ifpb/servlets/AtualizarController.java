package br.com.ifpb.servlets;

import br.com.ifpb.dao.AlbumDao;
import br.com.ifpb.modelo.Album;
import br.com.ifpb.modelo.Banda;
import br.com.ifpb.modelo.Estilo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

public class AtualizarController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AlbumDao albumDao = new AlbumDao();

        Album album = new Album();
        Banda banda = new Banda();

        banda.setNome(request.getParameter("bandaNovo"));
        album.setBanda(banda);

        album.setId(
                Integer.valueOf(request.getParameter("idBanda"))
        );

        album.setAnoDeLancamento(
                LocalDate.of(
                        Integer.parseInt(request.getParameter("anoDeLancamentoNovo")), 1, 1
                )
        );

        album.setEstilo(
                Estilo.valueOf(request.getParameter("estiloNovo"))
        );

        try {
            albumDao.update(album);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/listar");
        dispatcher.forward(request, response);
    }
}
