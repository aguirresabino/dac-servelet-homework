package br.com.ifpb.servlets;

import br.com.ifpb.dao.AlbumDao;
import br.com.ifpb.modelo.Album;
import br.com.ifpb.modelo.Banda;
import br.com.ifpb.modelo.Estilo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

public class SalvarController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AlbumDao albumDao = new AlbumDao();

        Album album = new Album();
        Banda banda = new Banda();

        album.setEstilo(
                Estilo.valueOf(request.getParameter("estilo"))
        );

        banda.setNome(request.getParameter("banda"));
        album.setBanda(banda);

        album.setAnoDeLancamento(
            LocalDate.of(
                Integer.parseInt(request.getParameter("anoDeLancamento")), 1, 1
            )
        );

        albumDao.create(album);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/listar");
        dispatcher.forward(request, response);
    }

}
