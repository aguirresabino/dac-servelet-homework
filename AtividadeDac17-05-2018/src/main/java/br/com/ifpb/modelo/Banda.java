/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.modelo;

import java.util.List;

/**
 *
 * @author aguirresabino
 */
public class Banda {
    private String nome;
    private String localDeOrigem;
    private List<String> integrantes;

    public Banda(String nome, String localDeOrigem, List<String> integrantes) {
        this.nome = nome;
        this.localDeOrigem = localDeOrigem;
        this.integrantes = integrantes;
    }
    
    public Banda(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLocalDeOrigem() {
        return localDeOrigem;
    }

    public void setLocalDeOrigem(String localDeOrigem) {
        this.localDeOrigem = localDeOrigem;
    }

    public List<String> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(List<String> integrantes) {
        this.integrantes = integrantes;
    }
}
