<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Atividade Dac</title>
</head>
<body>
    
    <form method="POST" action="salvar">
        <h3>Salvar álbum</h3>
        <label for="banda">Banda</label>
        <input type="text" id="banda" name="banda">
        <label for="ano">Ano de Lançamento</label>
        <input type="text" id="ano" name="anoDeLancamento">
        <label for="estilo">Estilo</label>
        <input type="text" id="estilo" name="estilo">
        <input type="submit">
    </form>
    <hr>
    
    <h3>Ver álbuns</h3>
    <a href="listar">Listar</a>
    <br/>
    <table>
        <thead>
            <tr>
                <th>Id do Album</th>
                <th>Nome da Banda</th>
                <th>Ano de Lancamento</th>
                <th>Estilo</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="album" items="${sessionScope.albuns}">
                <tr>
                    <td>${album.id}</td>
                    <td>${album.banda.nome}</td>
                    <td>${album.anoDeLancamento}</td>
                    <td>${album.estilo}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <br/>

    <hr>
    <h3>Atualizar álbum</h3>
    <form method="POST" action="atualizar">
        <label for="idBanda">Id da Banda</label>
        <input type="number" min="0" id="idBanda" name="idBanda">
        <label for="bandaNovo">Banda</label>
        <input type="text" id="bandaNovo" name="bandaNovo">
        <label for="anoNovo">Ano de Lançamento</label>
        <input type="text" id="anoNovo" name="anoDeLancamentoNovo">
        <label for="estiloNovo">Estilo</label>
        <input type="text" id="estiloNovo" name="estiloNovo">
        <input type="submit">
    </form>
    <hr>
    
    <h3>Deletar</h3>
    <table>
        <thead>
        <tr>
            <th>Id do Album</th>
            <th>Nome da Banda</th>
            <th>Ano de Lancamento</th>
            <th>Estilo</th>
            <th>Ação</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="album" items="${sessionScope.albuns}">
            <tr>
                <td>${album.id}</td>
                <td>${album.banda.nome}</td>
                <td>${album.anoDeLancamento}</td>
                <td>${album.estilo}</td>
                <td><a href="deletar?idBanda=${album.id}">Deletar</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</body>
</html>
