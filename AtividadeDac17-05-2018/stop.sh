#!/usr/bin/env bash
docker kill banco
docker stop banco
docker kill tomcat
docker stop tomcat

docker rm tomcat
docker rm banco
docker rmi atividade-dac/postgres
docker rmi atividade-dac/tomcat
