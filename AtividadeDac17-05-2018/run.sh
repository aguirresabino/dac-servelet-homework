#!/usr/bin/env bash
cd postgres
docker build -t atividade-dac/postgres .

cd ..

mvn clean install


docker build -t atividade-dac/tomcat .


docker run -p 5432:5432 -d --name banco atividade-dac/postgres
docker run -p 8080:8080 -d --name tomcat --link banco:host-banco atividade-dac/tomcat